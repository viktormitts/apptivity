import "./ActivityListItem.css";
import StarRating from "./StarRating";
import NumberRating from "./NumberRating";
import ActivityView from "../ActivityView";
import React from "react";

class ActivityListItem extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.select(this.props.activity);
    }

    render() {
        return (
            <div className="ActivityListItem-box" onClick={this.handleClick}>
                <img className="ActivityListItem-image" src={this.props.activity.image} alt={this.props.activity.title}/>
                <div>
                    <span className="ActivityListItem-activity">
                        {this.props.activity.title}
                    </span>
                    <span className="ActivityListItem-review">
                        <span className="ActivityListItem-rating">
                            <NumberRating rating={this.props.activity.rating} />
                        </span>
                        <StarRating rating={this.props.activity.rating}/>
                        <span className="ActivityListItem-reviews">
                            1 review
                        </span>
                    </span>
                </div>
                <div className="ActivityListItem-address">
                    {this.props.activity.address}
                </div>
                <div className="ActivityListItem-name">
                    {this.props.activity.location}
                </div>
            </div>
        );
    }
};

export default ActivityListItem;
