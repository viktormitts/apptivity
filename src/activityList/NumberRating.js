function NumberRating(props) {
    // there has to be a better way
    const ratingString = props.rating === Math.floor(props.rating) ? `${props.rating}.0` : props.rating;

    return (
        <span>{ratingString}</span>
    )
}

export default NumberRating;