import ActivityListItem from "./ActivityListItem";
import "./ActivityList.css";
import Activity from "./Activity";
import activityImage from "./activity.jpg";


function ActivityList(props) {
    let activityJsx = props.activities.map((e, i) => <ActivityListItem activity={e} chosen={props.chosenActivity} select={props.setActivity}/>);
    return (
        <div className="ActivityList-list">
            {activityJsx}
        </div>
    );
}

export default ActivityList;
