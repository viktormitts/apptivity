import "./ActivitySlider.css";
import Activity from "./Activity";
import activityImage from "./activity.jpg";
import StarRating from "./StarRating";

function ActivitySliderItem(props) {
    return (
        <div>
        <div>
          <h1>{props.activity.title}</h1>
          <p>{props.activity.location}</p>
        </div>
        <img src={props.activity.image} alt={props.activity.title}/>
        </div>
    );
}

function ActivitySlider(props) {
    let activityJsx = props.activities.map((e, i) => <ActivitySliderItem activity={e}/>);
    return (
        <div className="slider">
            {activityJsx}
        </div>
    );
}

export default ActivitySlider;
