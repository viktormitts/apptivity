import "./StarRating.css";

function StarRating(props) {
    const wholeStars = Math.floor(props.rating);
    const styles = [];
    let i;
    for (i = 0; i < wholeStars; ++i) {
        styles[i] = {
            background: "#EFCE5B"
        };
    }
    if (i < 5) {
        const fraction = props.rating - wholeStars;
        const percentage = `${fraction * 100}%`;
        styles[i] = {
            background: `linear-gradient(to right, #EFCE5B ${percentage}, #BBBBBB ${percentage})`
        };
    }
    for (++i; i < 5; ++i) {
        styles[i] = {
            background: "#BBBBBB"
        };
    }
    return (
        <span>
            <span className="StarRating-star" style={styles[0]}></span>
            <span className="StarRating-star" style={styles[1]}></span>
            <span className="StarRating-star" style={styles[2]}></span>
            <span className="StarRating-star" style={styles[3]}></span>
            <span className="StarRating-star" style={styles[4]}></span>
        </span>
    )
}

export default StarRating;
