enum Weather {
    All = 1,
    Warm = 2, // over 10 degrees,
    Summer = 3,
    Winter = 4
}

interface Comment {
    user: string;
    comment: string;
    rating: number;
}

interface Activity {
    image: string;
    title: string;
    description: string;
    location: string;
    address: string;
    coordinates: [number, number]; // [longitude, latitude]
    hours: [number, number];
    price: [number, number];
    duration: [number, number];
    weather: Weather;
    participants: number;
    requirementList: Array<string>;
    tagList: Array<string>;
    commentList: Array<Comment>;
    rating: number;
}

export default Activity;