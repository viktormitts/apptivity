import React from 'react';

interface Props {
  click: (v: number) => void;
}

const OptsView: React.FC<Props> = ({
    click,
}) => {
    return (
      <div className="wbar topBar"> 
        Here you put settings 
        <button onClick={a => click(3)}> Go back </button>
      </div>
    );
};

export default OptsView
