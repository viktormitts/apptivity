import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './css/index.css';
import './css/App.css';
import Apptivity from './Apptivity';

ReactDOM.render(
  <Apptivity />,
  document.getElementById('root')
);
