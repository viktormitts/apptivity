import React, { Component } from "react";
import { Filter } from "./Apptivity";
import "./css/App.css";
import CheckboxButton from "./CheckboxButton";

type FilterViewProps = {
    filter: Filter,
    interests: Set<string>,
    tags: Set<string>,
    click: (v: number) => void,
    apply: (f: Filter) => void
};

// warning: this sucks
type FilterEnabled = {
    distance: boolean,
    price: boolean,
    interests: boolean,
    tags: boolean
};

type FilterViewState = {
    filter: Filter,
    filterEnabled: FilterEnabled
};

class FilterView extends Component<FilterViewProps, FilterViewState> {
    constructor(props: FilterViewProps) {
        super(props);
        this.state = {
            filter: {
                distance: props.filter.distance,
                price: props.filter.price,
                interests: new Set(props.filter.interests ?? props.interests),
                tags: new Set(props.filter.tags ?? props.tags)
            },
            filterEnabled: {
                distance: props.filter.distance != undefined,
                price: props.filter.price != undefined,
                interests: props.filter.interests != undefined,
                tags: props.filter.tags != undefined
            }
        }

        this.handleChangeInterest = this.handleChangeInterest.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleChangeTag = this.handleChangeTag.bind(this);
        this.handleChangeEnable = this.handleChangeEnable.bind(this);
        this.filteredFilters = this.filteredFilters.bind(this);
    }

    handleChangeInterest(e: React.ChangeEvent<HTMLInputElement>) {
        switch (this.state.filter.interests?.has(e.target.value)) {
            case undefined:
                this.state.filter.interests = new Set(this.props.interests);
                this.state.filter.interests.delete(e.target.value);
                this.setState({});
                break;
            case true:
                this.state.filter.interests.delete(e.target.value);
                this.setState({});
                break;
            case false:
                this.state.filter.interests.add(e.target.value);
                this.setState({});
                break;
            default:
                throw "how";
        }
    }

    handleChangePrice(e: React.ChangeEvent<HTMLInputElement>) {
        this.state.filter.price = Number(e.target.value);
        this.setState({});
    }

    handleChangeTag(e: React.ChangeEvent<HTMLInputElement>) {
        switch (this.state.filter.tags?.has(e.target.value)) {
            case undefined:
                this.state.filter.tags = new Set(this.props.tags);
                this.state.filter.tags.delete(e.target.value);
                this.setState({});
                break;
            case true:
                this.state.filter.tags.delete(e.target.value);
                this.setState({});
                break;
            case false:
                this.state.filter.tags.add(e.target.value);
                this.setState({});
                break;
            default:
                throw "how";
        }
    }

    handleChangeEnable(e: React.ChangeEvent<HTMLInputElement>) {
        this.state.filterEnabled[e.target.value] = !this.state.filterEnabled[e.target.value];
        this.setState({});
    }

    // there must be a better way to do this
    filteredFilters(): Filter {
        return {
            distance: this.state.filterEnabled.distance ? this.state.filter.distance : undefined,
            price: this.state.filterEnabled.price ? this.state.filter.price : undefined,
            interests: this.state.filterEnabled.interests ? this.state.filter.interests : undefined,
            tags: this.state.filterEnabled.tags ? this.state.filter.tags : undefined
        }
    }

    render() {
        const interestsRendered: JSX.Element[] = [];
        for (const i of this.props.interests) {
            interestsRendered.push(<CheckboxButton
                checked={this.state.filter.interests?.has(i) ?? true}
                value={i}
                onChange={this.handleChangeInterest}
                key={i}>
                    {i}
                </CheckboxButton>);
        }
        const tagsRendered: JSX.Element[] = [];
        for (const i of this.props.tags) {
            tagsRendered.push(<CheckboxButton
                checked={this.state.filter.tags?.has(i) ?? true}
                value={i}
                onChange={this.handleChangeTag}
                key={i}>
                    {i}
                </CheckboxButton>);
        }
        return (
            <div className="optionsMenu menuWithButtons fullscreen">
                <div>
                    <h1>
                        Filters
                    </h1>
                    <span className="inputLabelLeft">
                        <input type="checkbox" id="distance" value="distance" onChange={this.handleChangeEnable} checked={this.state.filterEnabled.distance}/>
                        <label htmlFor="distance">
                            Max distance from
                        </label>
                    </span>
                    <select>
                        <option>
                            Home
                        </option>
                    </select>
                    <input type="number" value={this.state.filter.distance}/>
                    <h2>
                        <input type="checkbox" id="interests" value="interests" onChange={this.handleChangeEnable} checked={this.state.filterEnabled.interests}/>
                        <label htmlFor="interests">
                            Your interests
                        </label>
                    </h2>
                    {interestsRendered}
                    <h2>
                        <input type="checkbox" id="price" value="price" onChange={this.handleChangeEnable} checked={this.state.filterEnabled.price}/>
                        Price
                    </h2>
                    <input type="number" onChange={this.handleChangePrice} value={this.state.filter.price} />
                    <h2>
                        <input type="checkbox" id="tags" value="tags" onChange={this.handleChangeEnable} checked={this.state.filterEnabled.tags}/>
                        <label htmlFor="tags">
                            Tags
                        </label>
                    </h2>
                    {tagsRendered}
                </div>
                <div className="bottomButtons">
                    <button className="big gray" onClick={a => this.props.click(3)}>Go back</button>
                    <button className="big" onClick={a => this.props.apply(this.filteredFilters())}>Apply filters</button>
                </div>
            </div>
        );
    }
}

export default FilterView;