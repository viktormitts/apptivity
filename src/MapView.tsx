import React from 'react'
import { ReactComponent as SettingsIcon } from './res/settings.svg'
import { ReactComponent as AddIcon } from './res/plus.svg'
import mapIcon from './res/map.png'
import filterIcon from './res/filter.png'
import menuIcon from './res/menu.png'
import Activity from './activityList/Activity.js'
import Map from './Map/';
import './css/MapView.css'

interface Props {
    setActivity: (a: Activity) => void,
    click: (v: number) => void,
    activities: Activity[]
}  

const MapView: React.FC<Props> = ({
    setActivity,
    click,
    activities
}) => {
    
    const markerClick = (a: Activity) => {
        setActivity(a);
    };

    return (
        <div className="MV-wrapper">
            <div className="MV-header">
                <div className="header-main">
                    <span>Activities</span>
                    <AddIcon className="svg-icon" onClick={() => click(6)} />
                </div>
                <div className="header-tail">
                    <SettingsIcon className="svg-icon" onClick={() => click(4)} />
                </div>
            </div>
            <div className="relevant">
                <Map
                    openActivity={markerClick}
                    mapType={google.maps.MapTypeId.ROADMAP}
                    mapTypeControl={false}
                    markers={activities}
                >
                </Map>
            </div>
            <div className="navbar">
                <img src={mapIcon} onClick={e => click(3)} />
                <img src={filterIcon} onClick={e => click(1)} />
                <img src={menuIcon} onClick={e => click(2)} />
            </div>
        </div>
    );
};

export default MapView
