import React from 'react';
import UserPrefActivities from './userPrefList/UserPrefActivities'
import './css/UserPrefView.css';

interface Props {
    click: (v: number) => void;
  }
  
  const userPrefView: React.FC<Props> = ({
      click,
  }) => {
      return (
        <div className="wbar topBar"> 
          <UserPrefActivities/>
          <button className="UserPrefActView-DescB"onClick={a => click(4)}> User Description </button>
        </div>
      );
  };

export default userPrefView