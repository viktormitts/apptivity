import React, { useState } from 'react'
import './css/ActivityFormView.css'
import placeholder from './res/placeholder.png'
import Activity from './activityList/Activity'
import { ReactComponent as Upload } from './res/upload.svg'
import { ReactComponent as Confirm } from './res/check.svg'
import { ReactComponent as Cancel } from './res/cancel.svg'

type FixedProps = {
    title: string,
    value: number,
    updateParent: (o: object) => void
}

const FixedForm: React.FC<FixedProps> = ({ title, value, updateParent }) => {
    const [fixed, setFixed] = useState(value)
    return (
        <div className="AFM-form-row">
            <span>
                Set {title}:
            </span>
            <span>
                <input type="number" value={fixed} onChange={(e) => {
                    setFixed(Number(e.currentTarget.value))
                    updateParent({ fixed: Number(e.currentTarget.value) })
                }} />
            </span>
        </div>
    )
}

type RangeProps = {
    value: [number, number],
    updateParent: (o: object) => void
}

const RangeForm: React.FC<RangeProps> = ({ value, updateParent }) => {
    const [min, setMin] = useState(value[0])
    const [max, setMax] = useState(value[1])
    return (
        <div>
            <div className="AFM-form-row">
                <span>
                    Min:
                </span>
                <span>
                    <input type="number" value={min} onChange={(e) => {
                        setMin(Number(e.currentTarget.value))
                        updateParent({ min: Number(e.currentTarget.value) })
                    }} />
                </span>
            </div>
            <div className="AFM-form-row">
                <span>
                    Max:
                </span>
                <span>
                    <input type="number" value={max} onChange={(e) => {
                        setMax(Number(e.currentTarget.value))
                        updateParent({ max: Number(e.currentTarget.value) })
                    }} />
                </span>
            </div>
        </div>
    )
}

type ActivityFormProps = {
    click: (v: number) => void;
    addActivity: (a: Activity) => void;
}

type ActivityFormState = {
    image: string,
    preview: string,
    title: string,
    description: string,
    location: string,
    address: string,
    priceOption: string,
    price: [number, number],
    durationOption: string,
    duration: [number, number],
    participantOption: string,
    participants: number,
    requirement: string,
    requirementList: Array<string>,
    tag: string,
    tagList: Array<string>
}

class ActivityFormView extends React.Component<ActivityFormProps, ActivityFormState> {
    constructor(props) {
        super(props)
        this.state = {
            image: './res/placeholder.png',
            preview: placeholder,
            title: 'Title',
            description: '',
            location: '',
            address: '',
            priceOption: '0',
            price: [0, 0],
            durationOption: '0',
            duration: [0, 0],
            participantOption: '0',
            participants: 1,
            requirement: '',
            requirementList: [],
            tag: '',
            tagList: []
        }

        this.changePreview = this.changePreview.bind(this)
        this.updatePrice = this.updatePrice.bind(this)
        this.showPriceForm = this.showPriceForm.bind(this)
        this.updateDuration = this.updateDuration.bind(this)
        this.showDurationForm = this.showDurationForm.bind(this)
        this.showParticipantForm = this.showParticipantForm.bind(this)
        this.addRequirement = this.addRequirement.bind(this)
        this.removeRequirement = this.removeRequirement.bind(this)
        this.showRequirements = this.showRequirements.bind(this)
        this.addTag = this.addTag.bind(this)
        this.showTags = this.showTags.bind(this)
        this.submitForm = this.submitForm.bind(this)
    }

    changePreview(event) {
        const file = event.target.files[0]
        const mock = URL.createObjectURL(file)
        this.setState({ preview: mock })
        this.setState({ image: file.name })

        /*const fd = new FormData()
        fd.append('file', file)
        axios.post('http://localhost:3001/api/files/upload', fd, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            const file = res.data.file
            if (file) {
                console.log(file.originalname)
                try {
                    //this.setState
                    const image = require(`./uploads/${file.originalname}`)
                } catch (e) {
                    console.log('fail')
                }
            }
        })*/
    }

    updatePrice(obj) {
        if (obj.hasOwnProperty('fixed')) {
            this.setState({ price: [obj.fixed, obj.fixed] })
        } else if (obj.hasOwnProperty('min')) {
            this.setState({ price: [obj.min, this.state.price[1]] })
        } else if (obj.hasOwnProperty('max')) {
            this.setState({ price: [this.state.price[0], obj.max] })
        }
    }

    showPriceForm() {
        if (this.state.priceOption === '1') {
            return <FixedForm title="price" value={this.state.price[0]} updateParent={this.updatePrice} />
        } else if (this.state.priceOption === '2') {
            return <RangeForm value={this.state.price} updateParent={this.updatePrice} />
        }
    }

    updateDuration(obj) {
        if (obj.hasOwnProperty('fixed')) {
            this.setState({ duration: [obj.fixed, obj.fixed] })
        } else if (obj.hasOwnProperty('min')) {
            this.setState({ duration: [obj.min, this.state.duration[1]] })
        } else if (obj.hasOwnProperty('max')) {
            this.setState({ duration: [this.state.duration[0], obj.max] })
        }
    }

    showDurationForm() {
        if (this.state.durationOption === '1') {
            return <FixedForm title="duration" value={this.state.duration[0]} updateParent={this.updateDuration} />
        } else if (this.state.durationOption === '2') {
            return <RangeForm value={this.state.duration} updateParent={this.updateDuration} />
        }
    }

    showParticipantForm() {
        if (this.state.participantOption === '2') {
            return (
                <div className="AFM-form-row">
                    <span>
                        Number of participants:
                    </span>
                    <span>
                        <input type="number" value={this.state.participants} onChange={(e) => {
                            this.setState({ participants: Number(e.currentTarget.value) })
                        }} />
                    </span>
                </div>
            )
        }
    }

    addRequirement() {
        if (this.state.requirement.length > 0 && !this.state.requirementList.includes(this.state.requirement)) {
            const list = [...this.state.requirementList, this.state.requirement]
            this.setState({
                requirement: '',
                requirementList: list
            })
        }
    }

    removeRequirement(requirement) {
        const list = this.state.requirementList.filter(r => r !== requirement)
        this.setState({ requirementList: list })
    }

    showRequirements() {
        return (
            <ul className="AFM-list">
                {this.state.requirementList.map(item => (
                    <li key={item}>
                        {item} <button onClick={() => this.removeRequirement(item)}>Remove</button>
                    </li>
                ))}
            </ul>
        )
    }

    addTag() {
        console.log('add', this.state.tag)
        if (this.state.tag.length > 0 && !this.state.tagList.includes(this.state.tag)) {
            const list = [...this.state.tagList, this.state.tag]
            this.setState({
                tag: '',
                tagList: list
            })
        }
    }

    removeTag(tag) {
        const list = this.state.tagList.filter(t => t !== tag)
        this.setState({ tagList: list })
    }

    showTags() {
        return (
            <ul className="AFM-list">
                {this.state.tagList.map(item => (
                    <li key={item}>
                        {item} <button onClick={() => this.removeTag(item)}>Remove</button>
                    </li>
                ))}
            </ul>
        )
    }

    submitForm(event) {
        const activity: Activity = {
            image: this.state.preview,
            title: this.state.title,
            description: this.state.description,
            location: this.state.location,
            address: this.state.address,
            coordinates: [0, 0],
            hours: [0, 0],
            price: this.state.price,
            duration: this.state.duration,
            weather: 1,
            participants: this.state.participants,
            requirementList: this.state.requirementList,
            tagList: this.state.tagList,
            commentList: [],
            rating: 0
        }
        this.props.addActivity(activity)
        this.props.click(3)
    }

    render() {
        return (
            <div className="AFM-wrapper">
                <div className="AFM-scrollable">
                    <div className="AFM-image">
                        <img src={this.state.preview} alt="Activity image" />
                    </div>
                    <label htmlFor="AFM-file-upload" className="AFM-image-upload">
                        <Upload className="AFM-icon" />
                    </label>
                    <input id="AFM-file-upload" type="file" accept="image/*" onChange={this.changePreview} />
                    <div className="AFM-form">
                        <div className="AFM-title">
                            <input type="text" value={this.state.title} onChange={(e) => {
                                this.setState({ title: e.currentTarget.value })
                            }} />
                        </div>
                        <div>
                            <div className="AFM-form-label">Description</div>
                            <div className="AFM-form-field">
                                <textarea value={this.state.description} onChange={(e) => {
                                    this.setState({ description: e.currentTarget.value })
                                }} />
                            </div>
                        </div>
                        <div>
                            <div className="AFM-form-label">Location name</div>
                            <div className="AFM-form-field">
                                <input type="text" value={this.state.location} onChange={(e) => {
                                    this.setState({ location: e.currentTarget.value })
                                }} />
                            </div>
                        </div>
                        <div>
                            <div className="AFM-form-label">Address</div>
                            <div className="AFM-form-field">
                                <input type="text" value={this.state.address} onChange={(e) => {
                                    this.setState({ address: e.currentTarget.value })
                                }} />
                            </div>
                        </div>
                        <div>
                            <div className="AFM-form-label">Price (euros)</div>
                            <div className="AFM-form-field">
                                <select value={this.state.priceOption} onChange={(e) => {
                                    this.setState({ priceOption: e.currentTarget.value })
                                }}>
                                    <option value="0">Free</option>
                                    <option value="1">Fixed price</option>
                                    <option value="2">Price range</option>
                                </select>
                            </div>
                            {this.showPriceForm()}
                        </div>
                        <div>
                            <div className="AFM-form-label">Duration (mins)</div>
                            <div className="AFM-form-field">
                                <select value={this.state.durationOption} onChange={(e) => {
                                    this.setState({ durationOption: e.currentTarget.value })
                                }}>
                                    <option value="0">Indefinite</option>
                                    <option value="1">Fixed duration</option>
                                    <option value="2">Duration range</option>
                                </select>
                                {this.showDurationForm()}
                            </div>
                        </div>
                        <div>
                            <div className="AFM-form-label">Participants</div>
                            <div className="AFM-form-field">
                                <select value={this.state.participantOption} onChange={(e) => {
                                    this.setState({
                                        participantOption: e.currentTarget.value,
                                        participants: Number(e.currentTarget.value) + 1
                                    })
                                }}>
                                    <option value="0">None required</option>
                                    <option value="1">Pair activity</option>
                                    <option value="2">Group activity</option>
                                </select>
                            </div>
                            {this.showParticipantForm()}
                        </div>
                        <div>
                            <div className="AFM-form-label">Requirements</div>
                            <div className="AFM-form-add">
                                <input type="text" value={this.state.requirement} onChange={(e) => {
                                    this.setState({ requirement: e.currentTarget.value })
                                }} />
                                <button onClick={this.addRequirement}>Add requirement</button>
                            </div>
                            {this.showRequirements()}
                        </div>
                        <div>
                            <div className="AFM-form-label">Tags</div>
                            <div className="AFM-form-add">
                                <input type="text" value={this.state.tag} onChange={(e) => {
                                    this.setState({ tag: e.currentTarget.value })
                                }} />
                                <button onClick={this.addTag}>Add tag</button>
                            </div>
                            {this.showTags()}
                        </div>
                    </div>
                </div>
                <div className="AFM-navbar">
                    <Cancel className="svg-icon" onClick={() => this.props.click(3)} />
                    <Confirm className="svg-icon" onClick={this.submitForm} />
                </div>
                <div>
                    <button onClick={a => this.props.click(3)}>Go back</button>
                </div>
            </div>
        )
    }
}

export default ActivityFormView;