import React, { useEffect, useState, Component } from 'react';
import HomeView from "./HomeView";
import MapView from "./MapView";
import OptsView from "./OptsView";
import MsgView from "./MsgView";
import FilterView from "./FilterView";
import Activity from "./activityList/Activity";
import UserPrefView from './UserPrefView';
import UserPrefActiView from './UserPrefActView';
import ActivityFormView from './ActivityFormView';
import ActivityView from './ActivityView';
import StartView from './StartView';
import { loadMapApi } from "./utils/GoogleMapsUtils";
import placeholder from './res/placeholder.png'
import data from './res/activities.json'

export type Filter = {
    distance?: number,
    interests?: Set<string>,
    price?: number,
    tags?: Set<string>
}

type ApptivityState = {
    activities: Activity[],
    activityTypes: Set<string>,
    tags: Set<string>,
    filter: Filter,
    view: number,
    mapsLoaded: boolean,
    selectedActivity?: Activity,
    selectedActivityTypes: Set<string>,
    selectedTags: Set<string>
};

function getPath(obj) {
    if (obj.hasOwnProperty('image')) {
        return require(`./res/activity-images/${obj.image}`).default
    } else {
        return placeholder
    }
}

const tagCountDict = {}

function countTags(tagList) {
    for (const t of tagList) {
        tagCountDict[t] !== undefined
            ? tagCountDict[t] = tagCountDict[t] + 1
            : tagCountDict[t] = 0
    }
}

const activityData = data.map(obj => {
    let mock: Activity = {
        image: getPath(obj),
        title: `${obj.title}`,
        description: 'No description',
        location: `${obj.location}`,
        address: `${obj.address}`,
        coordinates: [obj.coordinates[0], obj.coordinates[1]],
        hours: [Number(obj.hours[0]), Number(obj.hours[1])],
        price: [obj.price[0], obj.price[1]],
        duration: [Number(obj.duration[0]), Number(obj.duration[1])],
        weather: Number(obj.weather),
        participants: obj.participants,
        requirementList: [],
        tagList: [...obj.tags],
        commentList: [...obj.commentList],
        rating: obj.commentList[0].rating
    }

    countTags(mock.tagList)
    return mock
})

const items = Object.keys(tagCountDict).map(function (key) {
    return [key, tagCountDict[key]];
});

items.sort(function (first, second) {
    return second[1] - first[1];
});

const startViewTags: string[] = items.map(arr => arr[0])

class Apptivity extends Component<{}, ApptivityState> {
    constructor(props: {}) {
        super(props);
        const activityTypes = new Set<string>();
        const tags = new Set<string>();
        for (const a of activityData) {
            activityTypes.add(a.title);
            for (const t of a.tagList) {
                tags.add(t);
            }
        }
        const favoriteTags = localStorage.getItem("favoriteTags");
        const favoriteTagsParsed: string[] = favoriteTags != null ? JSON.parse(favoriteTags) : [];
        const favoriteActivityTypes = localStorage.getItem("favoriteActivityTypes");
        const favoriteActivityTypesParsed: string[] = favoriteActivityTypes != null ? JSON.parse(favoriteActivityTypes) : [];
        this.state = {
            activityTypes: activityTypes,
            tags: tags,
            activities: activityData,
            filter: {},
            view: 8,
            mapsLoaded: false,
            selectedActivity: undefined,
            selectedTags: new Set(favoriteTagsParsed),
            selectedActivityTypes: new Set(favoriteActivityTypesParsed)
        }

        this.Click = this.Click.bind(this);
        this.setFilter = this.setFilter.bind(this);
        this.filterViewApply = this.filterViewApply.bind(this);
        this.clearActivity = this.clearActivity.bind(this);
        this.addActivity = this.addActivity.bind(this);
        this.setTags = this.setTags.bind(this);
        this.userPrefsApply = this.userPrefsApply.bind(this);
        this.matchesTags = this.matchesTags.bind(this);
        
        const googleMapScript = loadMapApi();
        const lol = this
        googleMapScript.addEventListener('load', function () {
            lol.setState({
                mapsLoaded: true
            });
        });
    }


    Click(i: number) {
        this.setState({
            view: i,
            selectedActivity: undefined
        });
    }

    setFilter(filter: Filter) {
        this.setState({
            filter: filter
        });
    }

    filterViewApply(filter: Filter) {
        this.setFilter(filter);
        this.Click(3);
    }

    selectActivity(activity: Activity) {
        this.setState({
            selectedActivity: activity
        });
    }

    clearActivity() {
        this.setState({
            selectedActivity: undefined
        });
    }

    getActivity() {
        return this.state.selectedActivity;
    }

    addActivity(activity: Activity) {
        const list = [...this.state.activities, activity]
        console.log('add activity', activity)
        this.setState({
            activities: list
        })
    }

    setTags(tags: Set<string>) {
        this.setState({
            selectedTags: tags
        })

        if (tags.size > 0) {
            const newFilter = this.state.filter
            newFilter.tags = new Set(tags)
            this.setState({
                filter: newFilter
            })
        }
        
        localStorage.setItem("favoriteTags", JSON.stringify(Array.from(tags)));
    }

    setActivityTypes(activityTypes: Set<string>) {
        this.setState({
            selectedActivityTypes: activityTypes
        });
        localStorage.setItem("favoriteActivityTypes", JSON.stringify(Array.from(activityTypes)));
    }

    userPrefsApply(tags?: Set<string>, activityTypes?: Set<string>) {
        if (tags != undefined) {
            this.setTags(tags);
        }
        if (activityTypes != undefined) {
            this.setActivityTypes(activityTypes);
        }
    }

    matchesTags(a: Activity) {
        for (const tag of a.tagList) {
            if (this.state.filter.tags?.has(tag)) {
                return true
            }
        }
        return false
    }

    render() {
        // filter activities according to the filter. the current implementation
        // will do many wasteful comparisons if there are no filters..
        let filtered: Activity[] = []
        if (this.state.filter.tags !== undefined && this.state.filter.tags.size > 0) {
            filtered = this.state.activities.filter(a => this.matchesTags(a))
        } else {
            filtered = this.state.activities.filter(a =>
                (this.state.filter?.interests?.has(a.title) ?? true) &&
                ((this.state.filter?.price ?? Number.MAX_SAFE_INTEGER) >= a.price[0]) // using minimum price
            )
        }

        const views = [
            (<MapView click={this.Click} activities={filtered} setActivity={this.selectActivity.bind(this)} />),
            (<OptsView click={this.Click} />),
            (<MsgView click={this.Click} />),
            (<HomeView click={this.Click} activities={filtered} setActivity={this.selectActivity.bind(this)} />),
            (<UserPrefView click={this.Click} activityTypes={this.state.activityTypes} tags={this.state.tags} apply={this.userPrefsApply} favoriteTags={this.state.selectedTags} favoriteActivityTypes={this.state.selectedActivityTypes} />),
            (<UserPrefActiView click={this.Click} />),
            (<ActivityFormView click={this.Click} addActivity={this.addActivity} />),
            (<FilterView click={this.Click} apply={this.filterViewApply} filter={this.state.filter} interests={this.state.selectedActivityTypes} tags={this.state.selectedTags} />),
            (<StartView click={this.Click} startTags={startViewTags} setTags={this.setTags} />)
        ]

        return (
            <div className="App">
                <ActivityView activity={this.state.selectedActivity} click={this.clearActivity} />
                {this.state.view !== 3 &&
                    <div className="overlay">
                        {views[this.state.view]}
                    </div>}
                {views[3]}
            </div>
        );
    }
}

export default Apptivity
