import React from 'react'
import PrefActivityItem from './PrefActivityItem'

const UserPrefActivities = (props) => {

    return (
        <div className="UserPrefDesc">
            <div className="UserPrefHeader">
                <h1>User preferences</h1>
                This information will be used to find the best activity for you. It can be changed later.
            </div>
            <PrefActivityItem activityTypes={props.activityTypes} apply={props.apply} favoriteActivityTypes={props.favoriteActivityTypes}/>
        </div>
    );
}


export default UserPrefActivities