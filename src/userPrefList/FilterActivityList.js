import React from 'react'
import { getAllJSDocTags } from 'typescript'

const FilterActivityList = (props) => {
    return (
        <div>
            <p>Please search and select an activity as your favorite.</p>
            <input className="PrefActivityItem-filterInput" value = {props.filters} onChange = {props.filterChangeH} placeholder="Search activities" />
            <div className = "PrefActivityItem-actListBox">
                {props.sActivities.map(activity => (
                    <li key = {activity} data-id = {activity} className = "PrefActivityItem-activityList" onClick={props.addActH}>
                        {activity}
                    </li>
                ))}
            </div>
        </div>
    )
}


export default FilterActivityList