import React from 'react'

const FavActList = (props) => {

    return (
    <div className="favorites">
        <h1>Your current favorites</h1>
        {props.favoriteActivityTypes.length > 0 &&
        <div>
            <p>Click on the activities, if you wish to remove them.</p> 
            <div className = "PrefActivityItem-favListBox">
                <nav>
                    {props.favoriteActivityTypes.sort().map(favorite => (
                        <li key = {favorite} data-id = {favorite} className = "PrefActivityItem-favoriteList" onClick={props.removeFavH}>
                            {favorite}
                        </li>
                    ))}
                </nav>
            </div>
        </div>
        }
        {props.favoriteActivityTypes.length === 0 &&
        <div>
            <p>You do not have any favorite activities.</p>
        </div>
        }
    </div>
    )
}


export default FavActList