import React, { useState } from 'react';
import UserPrefForm from './UserPrefForm';
import FilterTag from './FilterTag';

const UserPrefDesc = (props) => {
    const tags = Array.from(props.tags).sort();
    const [filter, setFilter] = useState("")
    const showedTags = tags.filter(tag => tag.toLowerCase().startsWith(filter.toLowerCase()))

    const handleFilterChange = (event) => {
        event.preventDefault()
        setFilter(event.target.value)
    }

    const addFavorite = (event) => {
        event.preventDefault();
        if (!props.favoriteTags.has(event.currentTarget.getAttribute("data-id"))) {
            const newFav = new Set(props.favoriteTags);
            newFav.add(event.currentTarget.getAttribute("data-id"));
            props.apply(newFav);
        }
    }

    const removeFavorite = (event) => {
        event.preventDefault();
        if (props.favoriteTags.has(event.currentTarget.getAttribute("data-id"))) {
            const newFavorites = new Set(props.favoriteTags);
            newFavorites.delete(event.currentTarget.getAttribute("data-id"));
            props.apply(newFavorites);
        }
    }

    return (
        
        <div>
            <div className="userPrefHeader">
                <h1>User preferences</h1>
                This information will be used to find the best activity for you. It can be changed later.
            </div>
            <div className="userDescription">
                <h1>Choose your favorite tags</h1>
                <FilterTag filters = {filter} filterChangeH = {handleFilterChange} tags = {showedTags} addTagH = {addFavorite}/>
                <UserPrefForm favoriteTags = {Array.from(props.favoriteTags)} removeTagH = {removeFavorite}/>
            </div>

        </div>
    );
}

export default UserPrefDesc