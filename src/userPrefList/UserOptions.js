import React from 'react';

//Dropdown table
const UserOptions = (props) => {
    const options = props.options
    return(
        <div className="descriptionForm">
            <select name={props.name}>
                {options.map(o => <option key={o} value="option">{o}</option>)}
            </select>
        </div>
    )
}

export default UserOptions