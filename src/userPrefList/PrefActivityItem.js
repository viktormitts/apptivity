import React, { useState } from 'react'
import './PrefActivityItem.css'
import FavActList from './FavActList'
import FilterActivityList from './FilterActivityList'

const PrefActivityItem = (props) => {
        const activities = Array.from(props.activityTypes).sort();
        const [filter, setFilter] = useState("");
        const showedActivities = activities.filter(activity => activity.toLowerCase().startsWith(filter.toLowerCase()));

        console.log(localStorage)
        const handleFilterChange = (event) => {
            event.preventDefault()
            setFilter(event.target.value)
        }

        const addActivity = (event) => {
            event.preventDefault();
            if (!props.favoriteActivityTypes.has(event.currentTarget.getAttribute("data-id"))) {
                const newFav = new Set(props.favoriteActivityTypes);
                newFav.add(event.currentTarget.getAttribute("data-id"));
                props.apply(undefined, newFav);
            }
        }

        const removeFavorite = (event) => {
            event.preventDefault();
            if (props.favoriteActivityTypes.has(event.currentTarget.getAttribute("data-id"))) {
                const newFavorites = new Set(props.favoriteActivityTypes);
                newFavorites.delete(event.currentTarget.getAttribute("data-id"));
                props.apply(undefined, newFavorites);
            }
        }

        return (
            <div>
                <div className="chooseActivity">
                    <h1>Choose your favorite activities</h1>
                </div>
                <FilterActivityList filters ={filter} filterChangeH = {handleFilterChange} sActivities = {showedActivities} addActH = {addActivity} activityTypes={activities}/>
                <FavActList favoriteActivityTypes = {Array.from(props.favoriteActivityTypes)} removeFavH = {removeFavorite} />
            </div>
        )
    
}


export default PrefActivityItem
