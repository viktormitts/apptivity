import React from 'react'

const FilterTag = (props) => {
    return (
        <div>
            <p>Please search and select tags as your favorites.</p>
            <input className="UserPrefForm-filterInput" value = {props.filters} onChange = {props.filterChangeH} placeholder="Search tags" />
            <div className = "UserPrefForm-TagListBox">
                {props.tags.map(tag => (
                    <li key = {tag} data-id = {tag} className = "UserPrefForm-TagList" onClick={props.addTagH}>
                        {tag}
                    </li>
                ))}
            </div>
        </div>
    )
}


export default FilterTag