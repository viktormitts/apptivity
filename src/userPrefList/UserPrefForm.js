import React from 'react';
import './UserPrefForm.css'

const UserPrefForm = (props) => {


    return (
        <div className = "UserPrefForm-FavoriteTagBox">
            <h1>Your current favorite tags</h1>
            {props.favoriteTags.length > 0 &&
            <div>
                <p>Click on the tags, if you wish to remove them.</p>
                <div className = "UserPrefForm-TagBox">
                    {props.favoriteTags.map(favorite => (
                        <button key = {favorite} data-id = {favorite} className = "UserPrefForm-TagButton" onClick={props.removeTagH}>
                            {favorite}
                        </button>
                    ))}
                </div>
            </div>
            }
            {props.favoriteTags.length === 0 &&
            <div>
                <p>You have not selected any favorite tags.</p>
            </div>
            }
        </div>
    )
}

export default UserPrefForm