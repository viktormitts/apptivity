import React, { useState}  from 'react';
import UserPrefDesc from './userPrefList/UserPrefDesc';
import UserPrefActivities from './userPrefList/UserPrefActivities'
import './css/UserPrefView.css';

interface Props {
    click: (v: number) => void;
    activityTypes: Set<string>;
    favoriteActivityTypes: Set<string>;
    tags: Set<string>;
    favoriteTags: Set<string>;
    apply: (tags?: Set<string>, activityTypes?: Set<string>) => void;
  }
  
  const UserPrefView: React.FC<Props> = ({
      click,
      activityTypes,
      tags,
      apply,
      favoriteTags,
      favoriteActivityTypes
  }) => {
      const [activities, setActivities] = useState(false);

      return (
        <div className="wbar topBar">
          {activities ?
            <UserPrefActivities activityTypes={activityTypes} apply={apply} favoriteActivityTypes={favoriteActivityTypes}/>
            : <UserPrefDesc tags={tags} favoriteTags={favoriteTags} apply={apply}/>}
          <button className="UserPrefView-homeB"onClick={a => click(3)}> Home </button>
          {activities ?
            <button className="UserPrefActView-DescB"onClick={a => setActivities(false)}> User Description </button> :
            <button className="UserPrefView-favoritesB" onClick={a => setActivities(true)}> Favorite activities</button>}
        </div>
      );
  };

export default UserPrefView