import Activity from './activityList/Activity'
import { ReactComponent as Star } from './res/star.svg'
import { ReactComponent as LeftArrow } from './res/left-arrow.svg'
import './css/ActivityView.css'

function getStarClasses(rating) {
    const stars = Math.round(rating)
    const classes: Array<string> = []
    for (let i = 0; i < stars; i++) { classes.push('gold-star') }
    for (let i = 0; i < 5 - stars; i++) { classes.push('grey-star') }
    return classes
}

const padded = (number) => {
    if (number < 10) {
        return `0${number}`
    } else {
        return `${number}`
    }
}

const showHours = (activity) => {
    if (activity.hours[0] === 0 && activity.hours[1] === 24) {
        return <span className="AV-space">Open: All day</span>
    } else {
        return <span className="AV-space">Open: {padded(activity.hours[0])}–{padded(activity.hours[1])} h</span>
    }
}

const showPrice = (activity) => {
    if (activity.price[0] === activity.price[1]) {
        if (activity.price[0] > 0) {
            return <span>Price: {activity.price[0]} €</span>
        } else {
            return <span>Price: Free</span>
        }
    } else {
        return <span>Price: {activity.price[0]}–{activity.price[1]} €</span>
    }
}

const showDuration = (activity) => {
    if (activity.duration[0] === activity.duration[1]) {
        if (activity.duration[0] > 0) {
            return <span>Duration: {activity.duration[0]} min</span>
        } else {
            return <span>Duration: Indefinite</span>
        }
    } else {
        return <span>Duration: {activity.duration[0]}–{activity.duration[1]} min</span>
    }
}

const showRating = (activity) => {
    const classes = getStarClasses(activity.rating)
    return (
        <div className="AV-rating">
            <span>{activity.rating.toFixed(1)}</span>
            <div className="AV-rating-stars">
                {classes.map(i => (
                    <Star className={i} />
                ))}
            </div>
            <span>{activity.commentList.length} reviews</span>
        </div>
    )
}

const getReviewRating = (rating) => {
    const classes = getStarClasses(rating)
    return (
        <div className="AV-review-rating">
            <span>{rating.toFixed(1)}</span>
            <div className="AV-review-stars">
                {classes.map(i => (
                    <Star className={i} />
                ))}
            </div>
        </div>
    )
}

const showReviews = (activity) => {
    return (
        <div>
            {activity.commentList.map(comment => (
                <div key={comment.user} className="AV-review">
                    <div className="AV-review-head">
                        <span>{comment.user}</span>
                        {getReviewRating(comment.rating)}
                    </div>
                    <span>{comment.comment}</span>
                </div>
            ))}
        </div>
    )
}

type ActivityViewProps = {
    activity?: Activity,
    click: () => any
}

const ActivityView: React.FC<ActivityViewProps> = ({ activity, click }) => {
    if (activity) {
        return (
            <div className="AV-wrapper">
                <div className="AV-image">
                    <img src={activity.image} alt="Activity image" />
                </div>
                <div className="AV-content">
                    <div className="AV-title">{activity.title}</div>
                    {showRating(activity)}
                    <div className="AV-location">
                        <span>{activity.address}</span>
                        <span>{activity.location}</span>
                    </div>
                    {showHours(activity)}
                    {showPrice(activity)}
                    {showDuration(activity)}
                    <div className="AV-subtitle">Photos</div>
                    <div className="AV-subtitle">Reviews</div>
                    {showReviews(activity)}
                </div>
                <div className='navbar-fixed' onClick={click}>
                    <LeftArrow className="svg-icon" />
                </div>
            </div>
        )
    } else {
        return null
    }
}

export default ActivityView
