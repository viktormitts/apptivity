import React, { useState } from 'react'
import { ReactComponent as SettingsIcon } from './res/settings.svg'
import { ReactComponent as AddIcon } from './res/plus.svg'
import { ReactComponent as Star } from './res/star.svg'
import mapIcon from './res/map.png'
import filterIcon from './res/filter.png'
import menuIcon from './res/menu.png'
import Activity from './activityList/Activity'
import './css/ListView.css'

import Map from './Map/'

function getStarClasses(rating) {
    const stars = Math.round(rating)
    const classes: Array<string> = []
    for (let i = 0; i < stars; i++) { classes.push('gold-star') }
    for (let i = 0; i < 5 - stars; i++) { classes.push('grey-star') }
    return classes
}

interface ListViewProps {
    click: (v: number) => void,
    activities: Activity[],
    setActivity: (a: Activity) => void
}

const ListView: React.FC<ListViewProps> = ({
    click,
    activities,
    setActivity
}) => {
    const markerClick = (a: Activity) => {
        setActivity(a)
    };

    const [map, setMap] = useState(false);


    const showDetails = (activity) => {
        setActivity(activity)
    }

    const showRating = (activity) => {
        const classes = getStarClasses(activity.rating)
        return (
            <div className="activity-rating">
                <span>{activity.rating.toFixed(1)}</span>
                <div className="activity-rating-stars">
                    {classes.map(i => (
                        <Star className={i} />
                    ))}
                </div>
                <span>{activity.commentList.length} reviews</span>
            </div>
        )
    }

    const showTags = (activity) => {
        return (
            <div className="tag-list">
                {activity.tagList.map(t => (
                    <span key={t}>{t}</span>
                ))}
            </div>
        )
    }

    const showFeed = () => {
        if (activities.length > 0) {
            return (
                <div className="feed">
                    {activities.map(a => (
                        <div className="feed-item" onClick={() => showDetails(a)}>
                            <img className="feed-item-image" src={a.image} alt={a.title} />
                            <div className="feed-item-body">
                                <span className="feed-item-title">{a.title}</span>
                                {showRating(a)}
                                <span>{a.location}</span>
                                {showTags(a)}
                            </div>
                        </div>
                    ))}
                </div>
            )
        }
    }

    return (
        <div className="LV-wrapper">
            <div className="LV-header">
                <div className="header-main">
                    <span>Activities</span>
                    <AddIcon className="svg-icon" onClick={() => click(6)} />
                </div>
                <div className="header-tail">
                    <SettingsIcon className="svg-icon" onClick={() => click(4)} />
                </div>
            </div>
            {map ? <Map
                openActivity={markerClick}
                mapType={google.maps.MapTypeId.ROADMAP}
                mapTypeControl={false}
                markers={activities}

            /> : showFeed()}
            <div className={activities.length > 0 ? 'navbar' : 'navbar-fixed'}>
                <img src={mapIcon} onClick={e => setMap(!map)} />
                <img src={filterIcon} onClick={e => click(7)} />
                <img src={menuIcon} onClick={e => click(2)} />
            </div>
        </div>
    )
}



export default ListView
