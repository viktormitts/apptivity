import React, { FunctionComponent } from "react";
import "./css/CheckboxButton.css";

type CheckboxButtonProps = {
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    checked: boolean,
    value?: string
}

const CheckboxButton: FunctionComponent<CheckboxButtonProps> = ({ onChange, checked, children, value }) => {
    const id: string = Math.random().toString();
    const checkedStyle = {
        background: "#637ACB",
        color: "#FFFFFF"
    };
    const uncheckedStyle = {
        background: "rgba(99, 122, 203, 0.06)",
        color: "#637ACB"
    };
    return (
        <label className="input CheckboxButton-button" style={checked ? checkedStyle : uncheckedStyle} htmlFor={id}>
            <input type="checkbox" className="CheckboxButton-checkbox" id={id} onChange={onChange} value={value ?? "on"} checked={checked}/>
            {children}
        </label>
    );
}

export default CheckboxButton;
