import React, {useEffect, useRef, useState} from 'react';
import './Map.scss';
import Activity from '../activityList/Activity'
import mapIcon from '../res/mapIcon.svg'

interface IMap {
    openActivity: (Activity) => void;
    mapType: google.maps.MapTypeId;
    mapTypeControl?: boolean;
    markers: Activity[]
}

interface IMarker {
    address: string;
    latitude: number;
    longitude: number;
}

type GoogleLatLng = google.maps.LatLng;
type GoogleMap = google.maps.Map;
type GoogleMarker = google.maps.Marker;

const Map: React.FC<IMap> = ({ openActivity, mapType, mapTypeControl = false, markers}) => {

    const ref = useRef<HTMLDivElement>(null);
    const [map, setMap] = useState<GoogleMap>();
    const [marker, setMarker] = useState<IMarker>();

    const startMap = (): void => {
        if (!map) {
            defaultMapStart();
        }
    };
    useEffect(startMap, [map]);

    const defaultMapStart = (): void => {
        const defaultAddress = new google.maps.LatLng(60.16858542838601, 24.93266925754456460);
        initMap(12, defaultAddress);
    };

    const initEventListener = ():void => {
        if (map) {
            google.maps.event.addListener(map, 'click', function(e) {
                coordinateToAddress(e.latLng);
            })
        }
    };
    useEffect(initEventListener, [map]);

    const coordinateToAddress = async (coordinate: GoogleLatLng) => {
        const geocoder = new google.maps.Geocoder();
        await geocoder.geocode({ location: coordinate}, function (results, status) {
            if (status === 'OK') {
                setMarker({
                    address: results[0].formatted_address,
                    latitude: coordinate.lat(),
                    longitude: coordinate.lng()
                })
            }
        });
    };

    const addMarker = (activity: Activity): void => {
        const location = new google.maps.LatLng(activity.coordinates[0], activity.coordinates[1]);
        const marker:GoogleMarker = new google.maps.Marker({
            position: location,
            map: map,
            icon: { url: mapIcon, scaledSize: new google.maps.Size(20, 20) }
        });

        marker.addListener("click", () => {
            openActivity(activity);
        });
    };

    const initMap = (zoomLevel: number, address: GoogleLatLng): void => {
        if (ref.current) {
            setMap(
                new google.maps.Map(ref.current, {
                    zoom: zoomLevel,
                    center: address,
                    mapTypeControl: mapTypeControl,
                    streetViewControl: false,
                    rotateControl: false,
                    scaleControl: true,
                    fullscreenControl: false,
                    panControl: false,
                    zoomControl: true,
                    gestureHandling: 'auto',
                    mapTypeId: mapType,
                    draggableCursor: 'pointer',
                })
            );
        }
    };

    markers.forEach((e, i) => {
        addMarker(e);
    });

    return (
        <div className="map-container">
            <div ref={ref} className="map-container__map"></div>
        </div>
    );
};

export default Map;
