import React, { useState } from 'react';
import './css/StartView.css';
import { ReactComponent as RightArrow } from './res/right-arrow.svg';

type TagProps = {
    title: string,
    addTag: (t: string) => void,
    removeTag: (t: string) => void
}

type TagState = {
    active: boolean
}

class Tag extends React.Component<TagProps, TagState> {
    constructor(props) {
        super(props)
        this.state = {
            active: false
        }
        this.toggleClass = this.toggleClass.bind(this)
    }

    toggleClass() {
        this.setState({ active: !this.state.active })
        this.state.active
            ? this.props.removeTag(this.props.title)
            : this.props.addTag(this.props.title)
    }

    render() {
        return (
            <div className={this.state.active ? 'SV-tag-dark' : 'SV-tag-light'}
                onClick={this.toggleClass}
            >
                {this.props.title}
            </div>
        )
    }
}

interface StartViewProps {
    click: (v: number) => void;
    startTags: string[];
    setTags: (t: Set<string>) => void;
}

const StartView: React.FC<StartViewProps> = ({ click, startTags, setTags }) => {
    const [tagList, setTagList] = useState<Array<string>>([])

    const addTag = (tag) => {
        if (!tagList.includes(tag)) {
            setTagList(tagList.concat(tag))
        }
        console.log('add', tag)
    }

    const removeTag = (tag) => {
        const list = tagList.filter(t => t !== tag)
        setTagList(list)
        console.log('remove', tag)
    }

    const storeTags = () => {
        setTags(new Set(tagList))
        click(3)
    }

    const showTags = () => {
        return (
            <div className="SV-taglist">
                {startTags.map(t => (
                    <Tag key={t}
                        title={t}
                        addTag={() => addTag(t)}
                        removeTag={() => removeTag(t)} />
                ))}
            </div>
        )
    }

    return (
        <div className="SV-wrapper">
            <div className="SV-header">
                <span className="SV-title">Hello!</span>
                <span>Pick some of your interests to help us recommend activities to you.</span>
                <span className="SV-status">{tagList.length} categories selected.</span>
            </div>
            <div className="SV-scrollable">
                {showTags()}
            </div>
            <div className="SV-navbar" onClick={() => storeTags()}>
                <span>{tagList.length === 0 ? 'Skip to suggestions' : 'Browse suggestions'}</span>
                <RightArrow className="svg-icon" />
            </div>
        </div>
    )
}

export default StartView